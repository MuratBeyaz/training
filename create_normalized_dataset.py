import pandas as pd
import numpy as np

read = "Dataset/Raw/lead1train.csv"
write = "Dataset/4_seconds/norm4sec_train.csv"

def normalize(signal):
    norm = 50*(signal / np.linalg.norm(signal))
    return norm


df = pd.read_csv(read, header=None)

#############  Normalizing 2500 Dataset #############
"""norms = []
for i in range(len(df)):
    liste = df.iloc[i].tolist()
    last = liste.pop()
    liste_array = np.array(liste)
    liste_array = normalize(liste_array)
    liste = liste_array.tolist()
    [str(i) for i in liste]
    liste.append(last)
    norms.append(liste)

dft = pd.DataFrame(norms)"""

############# Normalizing 4 seconds Dataset ##########

four_secs = []
for i in range(len(df)):
    liste = df.iloc[i].tolist()
    fp = liste[:1000]
    sp = liste[1000:2000]
    last = liste[len(liste) - 1]
    fp_array = np.array(fp)
    sp_array = np.array(sp)
    fp_array = normalize(fp_array)
    sp_array = normalize(sp_array)
    fp = fp_array.tolist()
    sp = sp_array.tolist()
    fp.append(last)
    sp.append(last)
    four_secs.append(fp)
    four_secs.append(sp)

dft = pd.DataFrame(four_secs)
dft.to_csv(write, index=False, header=None)

